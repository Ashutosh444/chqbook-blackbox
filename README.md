Role Name
========
  * Blackbox 

Variable's
=========
   * blackbox_url: "https://chqbook-blackbox.s3.ap-south-1.amazonaws.com/blackbox_exporter-0.12.0.linux-amd64.tar.gz"
   * binary_dir: "/usr/local/bin"
   * blackbox_user: blackbox_exporter
   * blackbox_group: blackbox_exporter


